-- DROP DATABASE IF EXISTS twitch_slim;
-- CREATE DATABASE twitch_slim CHARACTER SET utf8mb4;
USE twitch_slim;

CREATE TABLE posteos(
    id smallint unsigned not null auto_increment primary key,
    titulo varchar( 100 ) not null,
    texto text,
    fecha_alta datetime,
    activo tinyint(1) not null default 1
);

CREATE TABLE usuarios(
    id smallint unsigned not null auto_increment primary key,
    email varchar( 100 ) not null unique,
    password varchar( 40 ) not null,
    fecha_alta datetime,
    activo tinyint(1) not null default 1
);

CREATE TABLE comentarios(
    id int unsigned not null auto_increment primary key,
    comentario text,
    fecha_alta datetime,
    activo tinyint(1) not null default 1,
    fkposteo smallint unsigned not null,
    fkusuario smallint unsigned not null,

    foreign key( fkposteo ) references posteos(id) on delete cascade ,
    foreign key( fkusuario ) references usuarios( id )
);

INSERT INTO
    usuarios 
SET 
    email='german@email.com',
    password=sha1('1234'),
    fecha_alta=NOW( );

INSERT INTO
    usuarios 
SET 
    email='usuario@email.com',
    password=sha1('1234'),
    fecha_alta=NOW( );

INSERT INTO posteos( titulo, texto, fecha_alta )
VALUES
( 'post 1', 'este es mi primer texto', NOW() ),
( 'otra publicación', 'este es el segundo texto', NOW() ),
( 'todo mal :(', 'no pude ayudar a joni porque no aparecía en pantalla su escritorio', NOW() );

INSERT INTO
    comentarios 
SET 
    comentario='No me resolviste el problema, terrible servicio, una sola estrella',
    fecha_alta=NOW( ),
    fkposteo=3,
    fkusuario=2;

INSERT INTO
    comentarios 
SET 
    comentario='comentario del post 2',
    fecha_alta=NOW( ),
    fkposteo=2,
    fkusuario=1;
    
/*
SELECT 
    columna1 AS c1,
    columna2 AS c2,
    columna3 AS c3
FROM 
    tabla AS t1
    [LEFT/RIGHT] JOIN tabla2 AS t2 ON t1.id = t2.fkcolumna
    [LEFT/RIGHT] JOIN tabla3 AS t3 ON t2.id = t3.fkcolumna
WHERE 
    t1.columna1 = 'saraza' 
    ( [AND/OR] columna2 < 100 )
    ( [AND/OR] columna3 > 100 )
GROUP BY cX
HAVING cX < 100
ORDER BY 1 [ASC/DESC]
LIMIT 10, 5
*/