<?php
require( __DIR__ . '/vendor/autoload.php' );

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

use App\Controllers\ContactoController;
use App\Controllers\BlogController as Blog;

$app = AppFactory::create( ); 

$twig = Twig::create( 'templates', ['cache' => false] );
$app->add( TwigMiddleware::create( $app, $twig ) ); 

$app->get( '/', 'App\Controllers\HomeController:index' );

$app->get( '/contacto', ContactoController::class . ':index' );
$app->post( '/contacto', ContactoController::class . ':enviar' );

$app->get( '/blog', Blog::class . ':index' );
$app->get( '/blog/leer', Blog::class . ':leer' );

$app->run( );