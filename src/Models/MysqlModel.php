<?php 
namespace App\Models;

use PDO;

class MysqlModel{
    protected static $tabla = 'dual'; //esto va a definir MODELO POR MODELO a qué tabla se le va a pedir cada consulta del CRUD

    public static function one( $id = 0 ){
        $consulta = "SELECT * FROM ". static::$tabla ." WHERE id='$id' ORDER BY 1 DESC";
        $resultados = self::execute( $consulta );


        return $resultados[0] ?? NULL;
    }

    public static function select( $where = '' ){
        $consulta = "SELECT * FROM ". static::$tabla ." $where ORDER BY 1 DESC";

        return self::execute( $consulta );
    }

    protected static function execute( $query_sql ){
        $cnx = new PDO( 'mysql:host=localhost;dbname=twitch_slim;charset=utf8mb4', 'root', '' );
        $cnx->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        
        $stm = $cnx->prepare( $query_sql );
        $resultados = $stm->execute( );

        $filas = [ ];
        while( $r = $stm->fetch( PDO::FETCH_ASSOC ) ){
            $filas[] = $r;
        }

        return $filas;
    }
}